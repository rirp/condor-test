//
//  HomeViewTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 27/04/21.
//

import XCTest
import ViewInspector
import SwiftUI
@testable import PokemonApp

extension HomeView: Inspectable {}
extension LoadingView: Inspectable {}
extension TabView: Inspectable {}

class HomeViewTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHomeView_WhenGetAError_ShouldShowErrorView() throws {
        let mock = HomeViewModelMock()
        mock.error = "A error "
        mock.isShowingError = true
        mock.isLoadingShowing = false
        
        let sut = HomeView(viewModel: mock)
        let tabView = try sut
            .inspect()
            .find(viewWithTag: "MainTab")
            .tabView()
        
        XCTAssertNotNil(tabView)
    }

}
