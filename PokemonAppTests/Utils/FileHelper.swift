//
//  FileHelper.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 21/04/21.
//

import Foundation


class FileHelper {
    
    class func readJsonAsData<T>(_ classType: T.Type) -> Data {
        let jsonString: String = readJsonAsString(classType)

        guard let jsonData = jsonString.data(using: .utf8) else {
            fatalError("Unable to convert UnitTestData.json to Data")
        }
        
        return jsonData
    }
    
    class func readJsonAsString<T>(_ classType: T.Type) -> String {
        let name = String(describing: classType)
        
        guard let pathString = Bundle(for: FileHelper.self).path(forResource: name, ofType: "json") else {
            fatalError("UnitTestData.json not found")
        }
        
        guard let jsonString = try? String(contentsOfFile: pathString, encoding: .utf8) else {
            fatalError("Unable to convert UnitTestData.json to String")
        }
        
        return jsonString
    }
}
