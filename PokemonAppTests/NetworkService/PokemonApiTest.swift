//
//  PokemonApiTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 11/04/21.
//

import XCTest
import Combine
@testable import PokemonApp

class PokemonApiTest: XCTestCase {
    var cancellable: AnyCancellable?
    var sut: PokemonService!
    
    override func setUpWithError() throws {
        self.sut = PokemonService(session: Mock.session())
    }
    
    override func tearDownWithError() throws {
        sut = nil
        cancellable = nil
        URLProtocolMock.stubResponseData = nil
        URLProtocolMock.response = nil
    }
    
    func testPokemonApi_WhenAPIReturnData_ReturnPokemonList() {
        URLProtocolMock.stubResponseData = Mock.generationData
        URLProtocolMock.response = Mock.successResponse
        
        let expectationFinished = expectation(description: "finished")
        let expectationReceive = expectation(description: "receiveValue")
        let expectationFailure = expectation(description: "failure")
        expectationFailure.isInverted = true
        
        cancellable = sut.fetchPokemons(by: .i)
            .sink(receiveCompletion: { completion in
                guard case .finished = completion else { return }
                expectationFinished.fulfill()
            },
            receiveValue: { response in
                XCTAssertNotNil(response, "The struct does not conform the data")
                XCTAssertEqual(response.pokemons?.count, 1)
                expectationReceive.fulfill()
            })
        wait(for: [expectationFinished, expectationReceive, expectationFailure], timeout: 3)
        cancellable?.cancel()
    }
    
    func testPokemonApi_whenAPIReturnsData_RuturnPokemonDetail() {
        URLProtocolMock.stubResponseData = Mock.pokemonDetailData
        URLProtocolMock.response = Mock.successResponse
        
        let expectationFinished = expectation(description: "finished")
        let expectationReceive = expectation(description: "receiveValue")
        let expectationFailure = expectation(description: "failure")
        expectationFailure.isInverted = true
        
        cancellable = sut.fetchPokemonDetail(by: "bulbasaur")
            .sink(receiveCompletion: { completion in
                guard case .finished = completion else { return }
                expectationFinished.fulfill()
            },
            receiveValue: { response in
                XCTAssertNotNil(response, "The struct does not conform the data")
                expectationReceive.fulfill()
            })
        wait(for: [expectationFinished, expectationReceive, expectationFailure], timeout: 3)
        cancellable?.cancel()
    }
}
