//
//  NetworkServiceTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 10/04/21.
//

import XCTest
import Combine
@testable import PokemonApp

class NetworkServiceTest: XCTestCase {
    private var sut: NetworkService!
    private var cancellable: AnyCancellable?
    private var request: URLRequest!
    
    override func setUp() {
        self.sut = NetworkServiceMock(session: Mock.session())
        let url = URL(string: "https://pokeapi.co/api/v2/pokemon?limit=40")!
        self.request = URLRequest(url: url)
    }
    
    override func tearDown() {
        sut = nil
        cancellable = nil
        request = nil
        URLProtocolMock.error = nil
        URLProtocolMock.response = nil
        URLProtocolMock.stubResponseData = nil
    }
    
    func testNetworkService_WhenGivenSuccessfullResponse_ReturnsSuccess() {
        // Arrange
        URLProtocolMock.stubResponseData =  Mock.pokemonData
        URLProtocolMock.response = Mock.successResponse
        
        let expectationFinished = expectation(description: "finished")
        let expectationFailure = expectation(description: "failure")
        let expectation = self.expectation(description: "Consume Web Service Response Expectation")
        expectationFailure.isInverted = true
        
        // Act
        self.cancellable = sut.execute(request, decodingType: Specie.self, retries: 2)
            .sink(receiveCompletion: { completion in
                switch(completion) {
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                    expectationFailure.fulfill()
                case .finished:
                    expectationFinished.fulfill()
                }
            },
            receiveValue: { response in
                // Assert
                XCTAssertEqual(response.name, "bulbasaur", "The object should return \"pokemon\" as name")
                expectation.fulfill()
            })
        
        self.wait(for: [expectation, expectationFailure, expectationFinished], timeout: 2)
        self.cancellable?.cancel()
    }
    
    func testNetworkService_WhenServiceGetInternalError_FailureShouldFulfill() {
        // Arrange
        URLProtocolMock.response = Mock.internalErrorResponse
        
        let expectation = self.expectation(description: "Consume Web Service Response Expectation")
        
        // Act
        self.cancellable = sut.execute(request, decodingType: Specie.self, retries: 2)
            .sink(receiveCompletion: { completion in
                
                guard case .failure(let encounteredError) = completion else { return }
                guard let error = encounteredError as? APIError else { return }
                XCTAssertEqual(error, APIError.responseUnsuccessful)
                XCTAssertNotNil(error.localizedDescription)
                expectation.fulfill()
                
            },
            receiveValue: { _ in })
        
        self.wait(for: [expectation], timeout: 1)
        self.cancellable?.cancel()
    }
    
    func testNetworkService_WhenServiceGetGeneralError_FailureShouldFulfill() {
        // Arrange
        URLProtocolMock.error = APIError.unexceptedError
        
        let finishExceptation = self.expectation(description: "It should not finish without error")
        let reciveExceptation = self.expectation(description: "It should not receive data")
        let expectation = self.expectation(description: "Catched an unexceptation error")
        
        finishExceptation.isInverted = true
        reciveExceptation.isInverted = true
        // Act
        self.cancellable = sut.execute(request, decodingType: Specie.self, retries: 2)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    finishExceptation.fulfill()
                case .failure( _ ):
                    expectation.fulfill()
                }
            },
            receiveValue: { _ in
                reciveExceptation.fulfill()
            })
        
        self.wait(for: [expectation, finishExceptation, reciveExceptation], timeout: 1)
        self.cancellable?.cancel()
    }
}
