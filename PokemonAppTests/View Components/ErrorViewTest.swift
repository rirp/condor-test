//
//  ErrorView.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import XCTest
//import ViewInspector
@testable import PokemonApp

class ErrorViewTest: XCTestCase {
    
    var sut: ErrorView!

    override func setUpWithError() throws {
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testErrorView_WhenMessageIsNil_TextShouldBeUnknownError() throws {
        
//        let sut = ErrorView(message: .constant(nil))
//
//        ViewHosting.host(view: sut)
//        let string = try sut.inspect().vStack().text(1).string()
//        XCTAssertEqual("Unknown Error", string)
    }
    
    func testErrorView_WhenMessageIsProvided_ShouldNoTFail() throws {
//        let value = "This is a error"
//        let sut = ErrorView(message: .constant(value))
//        let string = try sut.inspect().vStack().text(1).string()
//        XCTAssertEqual(value, string)
    }

}
