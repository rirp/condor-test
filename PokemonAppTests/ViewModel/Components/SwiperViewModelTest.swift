//
//  SwiperViewModelTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 1/05/21.
//

import XCTest
@testable import PokemonApp

class SwiperViewModelTest: XCTestCase {
    var sut: SwiperViewModel!
    var mockStore: PokemonStoreMock!
    
    override func setUpWithError() throws {
        mockStore = PokemonStoreMock()
        sut = SwiperViewModel(store: mockStore)
    }

    override func tearDownWithError() throws {
        mockStore = nil
        sut = nil
    }

    func testSwiperViewModel_WhenObservableIsNotSettedUp_ObservableShouldBeNil() throws {
        
        sut.getAll()
        
        XCTAssertNil(sut.votes)
    }
    
    func testSwiperViewModel_WhenObservableIsSettedUp_ObservableShouldNotBeNil() throws {
        sut.setup(votes: PokemonEntityObservable())
        sut.getAll()
        
        XCTAssertNotNil(sut.votes)
    }
    
    func testSwiperViewModel_WhenFavoriteIsAdded_ShouldBeNotEmpty() {
        sut.setup(votes: PokemonEntityObservable())
        
        sut.saveFavorite(pokemon: Specie(name: "Pikachu",
                                         image: Data(),
                                         _url: mockSpriteDefault))
        
        XCTAssertFalse(sut.votes!.votes.isEmpty)
    }
    
    func testSwiperViewModel_WhenFavoriteIsAdded_ShouldBeOne() {
        sut.setup(votes: PokemonEntityObservable())
        
        sut.saveFavorite(pokemon: Specie(name: "Pikachu",
                                         image: Data(),
                                         _url: mockSpriteDefault))
        
        XCTAssertEqual(1, sut.votes!.votes.count)
    }

}
