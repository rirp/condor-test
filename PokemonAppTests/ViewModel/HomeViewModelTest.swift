//
//  HomeViewModelTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 13/04/21.
//

import XCTest
@testable import PokemonApp

class HomeViewModelTest: XCTestCase {
    
    var sut: HomeViewModel!
    var pokemonService: PokemonServiceProtocol!

    override func setUpWithError() throws {
        pokemonService = PokemonServiceMock()
        sut = HomeViewModel(pokemonService: pokemonService)
        PokemonServiceMock.generationData = Mock.generationI
    }

    override func tearDownWithError() throws {
        PokemonServiceMock.generationData = nil
        PokemonServiceMock.error = nil
        pokemonService = nil
        sut = nil
    }

    func testHomeViewModel_WhenPokemonGenerationFetched_ReturnList() {
        sut.getPokemons(by: .i)
        XCTAssertNotNil(sut.generationData.collection[.i])
        XCTAssertEqual(sut.generationData.collection.count, 1, "The number of generation should be one")
    }
    
    func testHomeViewModel_WhenPokemonGenerationFetched_ReturnError() {
        PokemonServiceMock.error = APIError.unexceptedError
        sut.getPokemons(by: .i)
        XCTAssertNil(sut.generationData.collection[.i], "Any generation should be returned")
        XCTAssertNotNil(sut.error, APIError.unexceptedError.localizedDescription)
    }
    
    func testHomeViewModel_WhenAllGenerationAreFilled_ShouldCountFour() {
        sut.getPokemons(by: .i)
        XCTAssertEqual(sut.generationData.collection.count, 1, "The number of generation should be one")
        sut.getPokemons(by: .ii)
        XCTAssertEqual(sut.generationData.collection.count, 2, "The number of generation should be two")
        sut.getPokemons(by: .iii)
        XCTAssertEqual(sut.generationData.collection.count, 3, "The number of generation should be three")
        sut.getPokemons(by: .iv)
        XCTAssertEqual(sut.generationData.collection.count, 4, "The number of generation should be four")
    }
    
    func testHomeViewModel_WhenAllGenerationAreFilledAndAddAnotherValue_ShouldCountFour() {
        sut.getPokemons(by: .i)
        sut.getPokemons(by: .ii)
        sut.getPokemons(by: .iii)
        sut.getPokemons(by: .iv)
        
        // It added one more time Generation IV and it should be 4 
        sut.getPokemons(by: .iv)
        XCTAssertEqual(sut.generationData.collection.count, 4, "The number of generation should be 4 after add a repited value again")
    }
}
