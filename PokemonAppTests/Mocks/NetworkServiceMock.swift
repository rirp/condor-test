//
//  MockNetworkService.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 10/04/21.
//

import Foundation
import Combine
@testable import PokemonApp

class NetworkServiceMock: NetworkService {
    var session: URLSession
    
    init(session: URLSession) {
        self.session = session
    }
}
