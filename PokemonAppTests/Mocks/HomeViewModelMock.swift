//
//  HomeViewModelMock.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 27/04/21.
//

import Foundation
@testable import PokemonApp

class HomeViewModelMock: BaseViewModel  {

    var generationData: Collection = Collection()
}

extension HomeViewModelMock: HomeViewModelProtocol {

    func getPokemons(by generation: PKGeneration) {
        generationData.collection.updateValue([], forKey: .i)
    }
}
