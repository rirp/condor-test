//
//  PokemonServiceMock.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 13/04/21.
//

import Foundation
import Combine
@testable import PokemonApp

class PokemonServiceMock {
    static var error: Error?
    static var generationData: Generation!
    static var pokemonDetail: Pokemon!
}


extension PokemonServiceMock: PokemonServiceProtocol {
    func fetchPokemons(by generation: PKGeneration) -> AnyPublisher<Generation, Error> {
        let data = PokemonServiceMock.generationData ?? Generation()
        let publisher = CurrentValueSubject<Generation, Error>(data)
        
        if let error = PokemonServiceMock.error {
            publisher.send(completion: .failure(error))
        }
        
        return publisher.eraseToAnyPublisher()
    }
    
    func fetchPokemonDetail(by id: String) -> AnyPublisher<Pokemon, Error> {
        let data = PokemonServiceMock.pokemonDetail ?? Pokemon()
        let publisher = CurrentValueSubject<Pokemon, Error>(data)
        
        if let error = PokemonServiceMock.error {
            publisher.send(completion: .failure(error))
        }
        
        return publisher.eraseToAnyPublisher()
    }
}
