//
//  PokemonStoreMock.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 1/05/21.
//

import Combine
import Foundation
import CoreData
@testable import PokemonApp

class PokemonStoreMock {
    var pokemons: CurrentValueSubject<[PokemonEntity], Never> = CurrentValueSubject<[PokemonEntity], Never>([])
    
    var result: [PokemonEntity] = []
}

extension PokemonStoreMock: PokemonStoreProtocol {
    func getAll() {
        self.pokemons.send(result)
    }
    
    func save(_ pokemon: Specie, isFavorite: Bool) {
        let entity = PokemonEntity(pokemon, context:NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType))

        entity.name = pokemon.name
        entity.imageURL = pokemon.url
        entity.isFavorite = isFavorite
        
        result.append(entity)
        self.pokemons.send(result)
    }
    
    func delete(_ pokemon: Specie, completion: @escaping PersistenceCompletion) {
        
        result.removeAll(where: { $0.name == pokemon.name })
        
        self.pokemons.send(result)
    }
    
    
}
