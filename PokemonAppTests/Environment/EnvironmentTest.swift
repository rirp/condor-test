//
//  EnvironmentTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 11/04/21.
//

import XCTest
@testable import PokemonApp

class EnvironmentTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testEnvironment_WhenGetBaseURLFromInfo_GetAValue() {
        let baseURL = EnvironmentConfig.infoForKey(.baseURL)
        XCTAssertNotNil(baseURL)
    }
    
    func testEnvironment_WhenGetImgBaseURLFromInfo_ReturnAValue() {
        let baseURL = EnvironmentConfig.infoForKey(.imgBaseURL)
        XCTAssertNotNil(baseURL)
    }
}
