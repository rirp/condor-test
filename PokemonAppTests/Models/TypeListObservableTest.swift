//
//  TypeListObservableTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import XCTest
@testable import PokemonApp

class TypeListObservableTest: XCTestCase {

    var sut: TypeListObservable!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTypeListObservable_WhenTypesAreProvided_ShouldNotBeNil() throws {
        sut = TypeListObservable(types: mockTypes)
        XCTAssertNotNil(sut.types)
        XCTAssertFalse(sut.types.isEmpty)
    }
}
