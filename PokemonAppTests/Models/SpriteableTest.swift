//
//  SpriteTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import XCTest
@testable import PokemonApp

class SpriteableTest: XCTestCase {
    
    var sut: Spriteable!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        
    }

    func testSpriteable_WhenSpritableImplementationHasValue_ShouldNotBeEmpty() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let item = SpriteItem(frontDefault: "pikachu")
        sut = GenerationSpriteI(redBlue: item, yellow: item)
        XCTAssertFalse(sut.all.isEmpty)
        XCTAssertEqual(sut.all.count, 2)
    }

    func testSpritable_WhenSpritableItemIsEmpty_ShouldBeEmpty() {
        let item = SpriteItem(frontDefault: nil)
        sut = GenerationSpriteI(redBlue: item, yellow: item)
        XCTAssertTrue(sut.all.isEmpty)
    }

}
