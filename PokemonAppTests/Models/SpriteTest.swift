//
//  SpriteTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import XCTest
@testable import PokemonApp

class SpriteTest: XCTestCase {
    var sut: Sprite!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSprite_WhenSpriteGeneration1IsProvided_ShouldNotBeEmpty() throws {
        // Arrange
        let countGeneration = 11
        sut = Sprite(frontDefault: mockSpriteDefault, versions: mockVersionSprite)
        
        // Act
        let generationI = sut.by(.i)
        
        
        // Assert
        XCTAssertEqual(generationI.count, countGeneration)
    }
    
    func testSprite_WhenSpriteGeneration2IsProvided_ShouldNotBeEmpty() throws {
        // Arrange
        let countGeneration = 9
        sut = Sprite(frontDefault: mockSpriteDefault, versions: mockVersionSprite)
        
        // Act
        let generationI = sut.by(.ii)
        
        // Assert
        XCTAssertEqual(generationI.count, countGeneration)
    }
    
    func testSprite_WhenSpriteGeneration3IsProvided_ShouldNotBeEmpty() throws {
        // Arrange
        let countGeneration = 6
        sut = Sprite(frontDefault: mockSpriteDefault, versions: mockVersionSprite)
        
        // Act
        let generationI = sut.by(.iii)
        
        // Assert
        XCTAssertEqual(generationI.count, countGeneration)
    }
    
    func testSprite_WhenSpriteGeneration4IsProvided_ShouldNotBeEmpty() throws {
        // Arrange
        let countGeneration = 3
        sut = Sprite(frontDefault: mockSpriteDefault, versions: mockVersionSprite)
        
        // Act
        let generationI = sut.by(.iv)
        
        // Assert
        XCTAssertEqual(generationI.count, countGeneration)
    }
    
    func testSprite_WhenSpriteGeneration4IsProvided_ShouldBeEmpty() throws {
        // Arrange
        sut = Sprite(frontDefault: mockSpriteDefault,
                     versions: SpriteVersions(generationI: nil,
                                              generationII: nil,
                                              generationIII: nil,
                                              generationIV: nil))
        
        // Act
        let result = sut.by(.iv)
        
        // Assert
        XCTAssertEqual(0, result.count)
    }
    
}
