//
//  SpritesTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import XCTest
@testable import PokemonApp


class SpriteObservableTest: XCTestCase {
    
    var sut: SpriteObservable!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSprites_WhenSpriteIsProvided_ShouldHaveValues() throws {
        sut = SpriteObservable(sprite: mockSprite, color: "FFFFFF")
        XCTAssertNotNil(sut.color)
        XCTAssertNotNil(sut.sprite)
    }
    
    func testSprites_WhenSpriteIsNotProvided_ValuesShouldBeNil() throws {
        sut = SpriteObservable(sprite: nil, color: nil)
        XCTAssertNil(sut.color)
        XCTAssertNil(sut.sprite)
    }

}
