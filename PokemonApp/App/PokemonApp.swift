//
//  PokemonAppApp.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 11/04/21.
//

import SwiftUI

@main
struct PokemonApp: App {
    
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            HomeView(viewModel: HomeViewModel())
                .environment(
                    \.managedObjectContext, persistenceController.container.viewContext
                )
        }
    }
}
