//
//  PokemonApi.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 11/04/21.
//

import Foundation
import Combine

protocol PokemonApiProtocol {
    func fetchPokemons(by generation: Generation) -> AnyPublisher<PokemonGeneration, Error>
}

class PokemonService: NetworkService {
    var session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
}

extension PokemonService: PokemonApiProtocol {
    func fetchPokemons(by generation: Generation) -> AnyPublisher<PokemonGeneration, Error> {
        let request = ApiURL.generation(generation).request()
        
        return execute(request, decodingType: PokemonGeneration.self)
    }
}
