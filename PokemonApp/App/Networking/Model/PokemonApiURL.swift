//
//  ApiURL.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 11/04/21.
//

import Foundation

enum PokemonApiURL {
    case generation(PKGeneration)
    case image(String)
    case detail(String)
}

extension PokemonApiURL {

    func base() -> String {
        switch self {
        case .image(_):
            return EnvironmentConfig.infoForKey(.imgBaseURL)!
        default:
            return EnvironmentConfig.infoForKey(.baseURL)!
        }
    }
    
    func endpoint() -> String {
        let base = self.base()
        
        switch self {
        case .generation(let generation):
            return base.appending("/generation/\(generation.rawValue)")
        case .image(let pokemonId):
            return base.appending("/\(pokemonId).png")
        case .detail(let name):
            return base.appending("/pokemon/\(name)")
        }
    }
    
    func url() -> URL {
        let urlString = self.endpoint()
        let url = URL(string: urlString)
        return url!
    }
    
    func request() -> URLRequest {
        return URLRequest(url: self.url(),
                          cachePolicy: .reloadIgnoringLocalCacheData)
    }
}
