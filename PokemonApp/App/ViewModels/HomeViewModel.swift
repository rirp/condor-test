//
//  HomeViewModel.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 12/04/21.
//

import Foundation
import SwiftUI
import Combine

protocol HomeViewModelProtocol: BaseViewModelProtocol {
    
    var generationData: Collection { get set }
    
    func getPokemons(by generation: PKGeneration)
}

class HomeViewModel: BaseViewModel {
    private var cancellable: AnyCancellable?
    
    private var pokemonService: PokemonServiceProtocol!
    
    // MARK: Current values
    @Published var generationData =  Collection()
    
    init(pokemonService: PokemonServiceProtocol = PokemonService()) {
        self.pokemonService = pokemonService
    }
}

extension HomeViewModel: HomeViewModelProtocol {
    func getPokemons(by generation: PKGeneration) {
        if generationData.collection[generation] == nil {
            self.fetchPokemonGeneration(generation)
        }
    }
    
}

private extension HomeViewModel {
    private func insertOrUpdate(_ pokemons: [Specie], generation: PKGeneration) {
        self.generationData.collection.updateValue(pokemons, forKey: generation)
    }
    
    private func fetchPokemonGeneration(_ generation: PKGeneration){
        self.isLoadingShowing = true
        
        self.cancellable = self.pokemonService
            .fetchPokemons(by: generation)
            .sink { [weak self] completion in
                guard case .failure(let error) = completion else { return }
                
                self?.removeLoading()
                self?.showError(message: error.localizedDescription)
            } receiveValue: { [weak self] response in
                self?.removeLoading()
                
                if let pokemons = response.pokemons {
                    self?.insertOrUpdate(pokemons, generation: generation)
                }
            }
    }
}
