//
//  VotingViewModel.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 30/04/21.
//

import Foundation
import Combine

class VotingViewModel: BaseViewModel {
    
    @Published var isLikeShowing = LikeShowingObservable()
    
    private var store: PokemonStoreProtocol
    
    @Published var votes = PokemonEntityObservable()
    
    private var cancellable: AnyCancellable?
    
    init(store: PokemonStoreProtocol = PokemonStore.shared) {
        self.store = store
    }
}
