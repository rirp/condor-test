//
//  SwiperViewModel.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 30/04/21.
//

import SwiftUI
import Combine

class SwiperViewModel: BaseViewModel {
    
    private var store: PokemonStoreProtocol
    
    var votes: PokemonEntityObservable?
    
    private var cancellable: AnyCancellable?
    
    init(store: PokemonStoreProtocol = PokemonStore.shared) { 
        self.store = store
    }
    
    func saveFavorite(pokemon: Specie) {
        store.save(pokemon, isFavorite: true)
    }
    
    func getAll() {
        store.getAll()
        
    }
    
    func setup(votes: PokemonEntityObservable) {
        self.votes = votes
        cancellable = store.pokemons.sink { (pokemons) in
            self.votes?.votes = pokemons
        }
    }
}
