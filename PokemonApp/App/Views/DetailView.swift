//
//  DetailView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 17/04/21.
//

import SwiftUI

struct DetailView: View {
    @ObservedObject var viewModel: DetailViewModel
    
    @EnvironmentObject var collection: Collection
    
    private var columns: [GridItem] =
        Array(repeating: .init(.flexible()), count: 3)
    
    init(name: String) {
        self.viewModel = DetailViewModel(name: name)
    }
    
    var body: some View {
        LoadingView(isShowing: $viewModel.isLoadingShowing) {
            ScrollView {
                VStack {
                    if let sprite = viewModel.detail?.sprites,
                       let types = viewModel.detail?.types {
                        SpritesView(sprites: SpriteObservable(sprite: sprite,
                                                     color: types.first?.value()))
                            .frame(width: UIScreen.main.bounds.width,
                                   height: 300,
                                   alignment: .center)
                    }
                    
                    
                        VStack(alignment: .leading) {
                            attributesView(weight: self.viewModel.detail?.weight,
                                           height: self.viewModel.detail?.height)
                        }
                        TypesView(types: TypeListObservable(types: viewModel.detail?.types ?? []))

                    movesView(viewModel.detail?.moves)
                }
                .navigationBarTitle(viewModel.detail?.name.firstCapitalized ?? "", displayMode: .inline)
                .onAppear {
                    viewModel.fetchPokemonDetail()
                }
            }
        }
    }
    
    @ViewBuilder
    private func attributesView(weight: Int?, height: Int?) -> some View {
        HStack {
            Text("Weight:").bold()
            Text(String(weight ?? 0))
            
            Text("Height:").bold()
            Text(String(height ?? 0))
        }
    }
    
    @ViewBuilder
    private func movesView(_ moves: Moves?) -> some View {
        VStack {
            Text("Moves")
                .foregroundColor(Color.blue.opacity(0.8))
            LazyVGrid(columns: columns, alignment: .leading) {
                ForEach(moves ?? [], id: \.self) { move in
                    Text(move.move.name)
                        .font(.caption)
                }
            }
            .padding()
        }
        
    }
    
}

#if DEBUG
struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(name: "butterfree")
            .environmentObject(Collection())
    }
}
#endif
