//
//  VotingView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import SwiftUI

struct VotingView: View {    
    let heightScreen = UIScreen.main.bounds.height
    let widthScreen = UIScreen.main.bounds.width
    
    @EnvironmentObject var collection: Collection
    
    @State private var pushed: Bool = false
    @State private var pageIndex = 0
    @State var values: [Specie] = []
    
    @ObservedObject var viewModel = VotingViewModel()
    
    var content: some View {
        VStack {
            Spacer()
            
            SwiperView(
                pages: values,
                index: $pageIndex
            )
            .frame(width: widthScreen)
            
            
            Text("Double touch the image if you like it")
                .font(.footnote)
                .fontWeight(.light)
            
            Spacer()
            
            LottieView(name: "lottie_swipe", loopMode: .playOnce)
                .frame(height: heightScreen / 5)
        }
        
    }
    
    var body: some View {
        LikeView() {
            content
                .navigationBarItems(
                    trailing:
                        Button(action: { pushed.toggle() }, label: {
                            Text("Vote list")
                        })
                )
                .onAppear {
                    values = collection.collection
                        .values
                        .flatMap { $0 }
                        .shuffled()
                }
        }
        .sheet(isPresented: $pushed) {
            VoteListView()
        }
        .environmentObject(viewModel.votes)
        .environmentObject(viewModel.isLikeShowing)
    }
}

#if DEBUG
struct VotingView_Previews: PreviewProvider {
    static var previews: some View {
        VotingView()
    }
}
#endif
