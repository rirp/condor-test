//
//  VoteListView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 1/05/21.
//

import SwiftUI

struct VoteListView: View {
    @EnvironmentObject var votes: PokemonEntityObservable
    
    var body: some View {
        List(votes.votes, id: \.self) { vote in
            ImageLoadableView(
                url: URL(string: vote.imageURL!)!,
                placeholder: { ActivityIndicator() },
                image: { Image(uiImage: $0).resizable() }
            )
            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
            Text(vote.name ?? "")
        }
    }
}

#if DEBUG
struct VoteListView_Previews: PreviewProvider {
    static var previews: some View {
        VoteListView()
    }
}
#endif
