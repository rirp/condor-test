//
//  ContentView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 11/04/21.
//

import SwiftUI

struct HomeView<Model>: View where Model: HomeViewModelProtocol {
    @ObservedObject var viewModel: Model
    
    //internal var didAppear: ((Self) -> Void)?
    
    var floatinButton: some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                NavigationLink(destination: VotingView()) {
                    Image("icon_vote")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 80, height: 80)
                        .cornerRadius(38.5)
                        .padding()
                        .shadow(color: Color.black.opacity(0.3),
                                radius: 3,
                                x: 3,
                                y: 3)
                        .padding(.bottom, 50)
                }
                
            }
        }
    }
    
    var body: some View {
        LoadingView(isShowing: $viewModel.isLoadingShowing) {
            NavigationView {
                ZStack {
                    TabView(selection: $viewModel.generationData.generationSelected) {
                        ForEach(PKGeneration.allCases, id: \.self) { generation in
                            tabView(generation: generation)
                        }
                    }
                    .tag("MainTab")
                    .onReceive(viewModel.generationData.$generationSelected) { generation in
                        viewModel.getPokemons(by: generation)
                    }
                    .sheet(isPresented: $viewModel.isShowingError) {
                        ErrorView(message: $viewModel.error)
                    }
                    
                    floatinButton
                }
            }
            .environmentObject(viewModel.generationData)
        }
    }
    
    @ViewBuilder
    func tabView(generation: PKGeneration) -> some View {
        PokemonListView(generation: generation)
            .tabItem {
                Image(generation.rawValue)
                    .resizable()
                    .frame(width: 10, height: 10)
                Text(generation.rawValue.firstCapitalized)
                
            }
            .tag(generation)
    }
    
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(viewModel: HomeViewModel())
    }
}
#endif
