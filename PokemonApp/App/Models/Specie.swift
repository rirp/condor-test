//
//  Pokemon.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 10/04/21.
//

import Foundation

struct Specie: Hashable, Decodable {
    var name: String?
    var image: Data?
    var _url: String
    
    var url: String {
        guard let value = self._url.split(separator: "/").last else {
            return ""
        }
        
        return PokemonApiURL.image(String(value)).endpoint()
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case _url = "url"
    }
}
