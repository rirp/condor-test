//
//  TypeListObservable.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

class TypeListObservable: ObservableObject {
    @Published var types: [Type]
    
    init(types: [Type]) {
        self.types = types
    }
}
