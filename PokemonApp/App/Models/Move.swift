//
//  PokemonMove.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 24/04/21.
//

import Foundation

typealias Moves = [Move]

struct Move: Decodable, Hashable {
    let move: Item
}
