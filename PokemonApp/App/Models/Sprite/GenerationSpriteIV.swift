//
//  GenerationIV.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

struct GenerationSpriteIV: Codable, Spriteable {
    let diamondPearl: SpriteItem?
    let heartGoldSoulsilver: SpriteItem?
    let platinum: SpriteItem?
    
    enum CodingKeys: String, CodingKey {
        case diamondPearl           = "diamond-pearl"
        case heartGoldSoulsilver    = "heartgold-soulsilver"
        case platinum
    }
}

#if DEBUG || TESTING

let mockGenerationIVSprite = GenerationSpriteIV(diamondPearl: mockSpriteItem,
                                          heartGoldSoulsilver: mockSpriteItem,
                                          platinum: mockSpriteItem)

#endif
