//
//  Spritable.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

protocol Spriteable {
    var all: [String] { get }
}

extension Spriteable {
    var all: [String] {
        let mirror = Mirror(reflecting: self)
        
        return mirror.children.compactMap { val in
            if let item = val.value as? SpriteItem, let value = item.frontDefault {
                return value
            }
            
            return nil
        }
    }
}

