//
//  Sprite+ObservableObject.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

class SpriteObservable: ObservableObject {
    @Published var sprite: Sprite?
    @Published var color: String?
    
    init(sprite: Sprite?, color: String?) {
        self.sprite = sprite
        self.color = color
    }
}
