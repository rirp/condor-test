//
//  GenerationIII.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

struct GenerationSpriteIII: Codable, Spriteable {
    let emerald: SpriteItem?
    let fireredLeafgreen: SpriteItem?
    let rubySapphire: SpriteItem?
    
    enum CodingKeys: String, CodingKey {
        case emerald
        case fireredLeafgreen   = "firered-leafgreen"
        case rubySapphire       = "ruby-sapphire"
    }
}

#if DEBUG || TESTING

let mockGenerationIIISprite = GenerationSpriteIII(emerald: mockSpriteItem,
                                            fireredLeafgreen: mockSpriteItem,
                                            rubySapphire: mockSpriteItem)

#endif
