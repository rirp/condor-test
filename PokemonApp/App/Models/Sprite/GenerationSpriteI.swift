//
//  GenerationI.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

struct GenerationSpriteI: Codable, Spriteable {
    let redBlue: SpriteItem?
    let yellow: SpriteItem?
    
    enum CodingKeys: String, CodingKey {
        case redBlue = "red-blue"
        case yellow
    }
}

#if DEBUG || TESTING

let mockGenerationISprite = GenerationSpriteI(redBlue: mockSpriteItem,
                                        yellow: mockSpriteItem)

#endif
