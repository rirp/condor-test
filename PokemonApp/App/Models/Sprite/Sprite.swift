//
//  Sprites.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 24/04/21.
//

import Foundation

struct Sprite: Codable {
    let frontDefault: String?
    let versions: SpriteVersions?
    
    func by(_ generation: PKGeneration) -> [String] {
        guard versions != nil else { return [] }
        var values: [String] = []
        
        let generationI = versions?.generationI?.all ?? []
        let generationII = versions?.generationII?.all ?? []
        let generationIII = versions?.generationIII?.all ?? []
        let generationIV = versions?.generationIV?.all ?? []
        
        switch generation {
        case .ii:
            values = generationII + generationIII + generationIV
            break
        case .iii:
            values = generationIII + generationIV
            break;
        case .iv:
            values = generationIV
            break
        default:
            values = generationI + generationII + generationIII + generationIV
            break
        }
        
        return values
    }
    
    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
        case versions
    }
}

#if DEBUG || TESTING

let mockSprite = Sprite(frontDefault: mockSpriteDefault, versions: mockVersionSprite)

#endif
