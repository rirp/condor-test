//
//  CurrentIndex.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 25/04/21.
//

import SwiftUI

class CurrentIndex: ObservableObject {
    @Published var index: Int = 0
}
