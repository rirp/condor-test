//
//  Generation.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 11/04/21.
//

import Foundation
import SwiftUI

struct Generation: Decodable {
    
    /// The identifier for this generation resource
    var id: Int?
    
    /// The name for this generation resource
    var name: String?
    
    /// A list of Pokémon species that were introduced in this generation
    var pokemons: [Specie]?
    
    init(id: Int? = nil, name: String? = nil, pokemons: [Specie]? = nil) {
        self.id = id
        self.name = name
        self.pokemons = pokemons
    }
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case pokemons = "pokemon_species"
    }
}
