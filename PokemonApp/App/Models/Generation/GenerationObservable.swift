//
//  GenerationObservable.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

class Collection: ObservableObject {
    @Published var collection: [PKGeneration: [Specie]]
    
    @Published var generationSelected: PKGeneration

    init() {
        self.collection = [:]
        self.generationSelected = .i
    }
}
