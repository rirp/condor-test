//
//  PKGeneration.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

enum PKGeneration: String, CaseIterable, Hashable {
    case i      = "generation-i"
    case ii     = "generation-ii"
    case iii    = "generation-iii"
    case iv     = "generation-iv"
}
