//
//  PersistenceController.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 29/04/21.
//

import CoreData

typealias PersistenceCompletion = (Error?) -> ()

enum StorageType {
  case persistent, inMemory
}

struct PersistenceController {
    static let shared = PersistenceController()
    
    let container: NSPersistentContainer
    
    init(_ storageType: StorageType = .persistent) {
        container = NSPersistentContainer(name: "PokemonApp")
        
        if storageType == .inMemory {
            let description = NSPersistentStoreDescription()
            description.url = URL(fileURLWithPath: "/dev/null")
            container.persistentStoreDescriptions = [description]
        }
        
        container.loadPersistentStores { (_, error) in
            if let error = error {
                fatalError("Error \(error.localizedDescription)")
            }
        }
    }
    
    func save(completion: @escaping PersistenceCompletion = {_ in}) {
        let context = container.viewContext
        
        self.container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        if context.hasChanges {
            do {
                try context.save()
                completion(nil)
            } catch {
                completion(error)
            }
        }
    }
    
    func delete(_ object: NSManagedObject, completion: @escaping PersistenceCompletion = {_ in}) {
        let context = container.viewContext
        context.delete(object)
        save(completion: completion)
    }
}
