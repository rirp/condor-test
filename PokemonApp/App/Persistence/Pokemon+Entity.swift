//
//  Pokemon+Entity.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 29/04/21.
//

import CoreData

extension PokemonEntity {
    convenience init(_ pokemon: Specie,
                     context: NSManagedObjectContext) {
        self.init(context: context)
        
        self.name = pokemon.name
        self.imageURL = pokemon.url
    }
}
