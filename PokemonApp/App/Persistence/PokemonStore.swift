//
//  PokemonStore.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 29/04/21.
//

import Foundation
import Combine
import CoreData

protocol PokemonStoreProtocol {
    var pokemons: CurrentValueSubject<[PokemonEntity], Never> { get set }
    
    func getAll()
    
    func save(_ pokemon: Specie, isFavorite: Bool)
    
    func delete(_ pokemon: Specie, completion: @escaping PersistenceCompletion)
}

class PokemonStore: NSObject, ObservableObject {
    @Published var pokemons = CurrentValueSubject<[PokemonEntity], Never>([])
    
    static let shared = PokemonStore()
    
    private let pokemonsFetchController: NSFetchedResultsController<PokemonEntity>
    
    private override init() {
        let fetchRequest: NSFetchRequest<PokemonEntity> = PokemonEntity.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \PokemonEntity.id,
                                                         ascending: true)]
        pokemonsFetchController = NSFetchedResultsController (
            fetchRequest: fetchRequest,
            managedObjectContext: PersistenceController.shared.container.viewContext,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        
        super.init()
        
        pokemonsFetchController.delegate = self
        
        getAll()
    }
    
    
}

extension PokemonStore: PokemonStoreProtocol {
    
    func getAll() {
        do {
            try pokemonsFetchController.performFetch()
            pokemons.send(pokemonsFetchController.fetchedObjects ?? [])
        } catch {
            pokemons.send([])
        }
    }
    
    func save(_ pokemon: Specie, isFavorite: Bool) {
        
        let persistenceController = PersistenceController.shared
        
        /// Initilize Data
        let entity = PokemonEntity(pokemon, context: persistenceController.container.viewContext)
        entity.isFavorite = isFavorite
        
        /// Save in device
        persistenceController.save()
    }
    
    func delete(_ pokemon: Specie, completion: @escaping PersistenceCompletion) {
        let persistenceController = PersistenceController.shared
        
        let pokemonEntity = pokemons.value.filter { (entity) in
            return entity.name == pokemon.name
        }.first
        
        if let entity = pokemonEntity {
            persistenceController.delete(entity, completion: completion)
        } else {
            completion(APIError.unexceptedError)
        }
    }
}

extension PokemonStore: NSFetchedResultsControllerDelegate {
    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        guard let pokemons = controller.fetchedObjects as? [PokemonEntity] else { return }
        self.pokemons.send(pokemons)
    }
}
