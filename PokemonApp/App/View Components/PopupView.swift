//
//  PopupView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 20/04/21.
//

import SwiftUI

struct PopupView<Content>: View where Content: View {
    var popupContent: some View
    
    @Binding var isShowing: Bool
    
    var content: () -> Content
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                self.content()
                    .disabled(self.isShowing)
                    .blur(radius: self.isShowing ? 3 : 0)
                
                VStack {
                    LottieView(name: "pokemon_flying")
                }
                .frame(width: geometry.size.width,
                       height: geometry.size.height)
                
                .background(Color.green.opacity(0.3))
                .cornerRadius(10)
                .opacity(self.isShowing ? 1 : 0)
                .foregroundColor(Color.primary)
                .ignoresSafeArea()
            }
            .ignoresSafeArea()
        }
    }
}

struct PopupView_Previews: PreviewProvider {
    static var previews: some View {
        PopupView(popupContent: Text(""), isShowing: .constant(true)) {
            Text("")
        }
    }
}
