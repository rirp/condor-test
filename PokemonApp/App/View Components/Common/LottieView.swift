//
//  LottieView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 19/04/21.
//

import SwiftUI
import Lottie

struct LottieView: UIViewRepresentable {
    private var name: String
    private var callback: (Bool) -> Void
    
    private var loopMode: LottieLoopMode = .loop
    private let isPaused: Bool
    private var animationView = AnimationView()
    
    
    init(name: String,
         loopMode: LottieLoopMode = .loop,
         isPaused: Bool = false,
         callback: @escaping (Bool) -> Void = { _ in }) {
        self.name = name
        self.loopMode = loopMode
        self.isPaused = isPaused
        self.callback = callback
    }
    
    func makeUIView(context: UIViewRepresentableContext<LottieView>) -> UIView {
        let view = UIView(frame: .zero)
        
        animationView.animation = Animation.named(name)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = loopMode
        animationView.play { finished in
            callback(finished)
        }
        
        animationView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(animationView)
        
        NSLayoutConstraint.activate([
            animationView.heightAnchor.constraint(equalTo: view.heightAnchor),
            animationView.widthAnchor.constraint(equalTo: view.widthAnchor)
        ])
        
        return view
    }
    
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<LottieView>) {
        if isPaused {
            context.coordinator.parent.animationView.pause()
        } else {
            context.coordinator.parent.animationView.play{ finished in
                callback(finished)
            }
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject {
        var parent: LottieView
        
        init(_ parent: LottieView) {
            self.parent = parent
        }
    }
}
