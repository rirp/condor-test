//
//  AppLoading.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 17/04/21.
//

import SwiftUI

struct LoadingView<Content>: View where Content: View {
    
    @Binding var isShowing: Bool
    
    var content: () -> Content
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                self.content()
                    .disabled(self.isShowing)
                    .blur(radius: self.isShowing ? 3 : 0)
                
                Color.green.opacity(0.3)
                    .overlay(PikachuView())
                    .opacity(self.isShowing ? 1 : 0)
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity                        
                    )
                    .edgesIgnoringSafeArea(.all)
            }
        }
    }
}

#if DEBUG
struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView(isShowing: .constant(true)) {
            Text("")
        }
    }
}
#endif
