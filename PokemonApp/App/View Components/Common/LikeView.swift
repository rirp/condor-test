//
//  LikeView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 30/04/21.
//

import SwiftUI

class LikeShowingObservable: ObservableObject {
    @Published var isLikeShowing: Bool = false
}

struct LikeView<Content: View> : View {
    
    @EnvironmentObject var isLikeShowing : LikeShowingObservable
    
    var content: () -> Content
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                self.content()
                    .disabled(self.isLikeShowing.isLikeShowing)
                    .blur(radius: self.isLikeShowing.isLikeShowing ? 3 : 0)
                
                VStack {
                    Spacer()
                    
                    LottieView(
                        name: "lottie_like",
                        loopMode: .playOnce,
                        isPaused: !self.isLikeShowing.isLikeShowing
                    ) { isFinished in
                        if isFinished {
                            self.isLikeShowing.isLikeShowing.toggle()
                        }
                    }
                    
                    Spacer()
                }
                .opacity(self.isLikeShowing.isLikeShowing ? 1 : 0)
            }
        }
    }
}
