//
//  ImageCarrouselView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 24/04/21.
//

import SwiftUI
import Combine

struct ImageCarouselView<Content: View>: View {
    
    private var numberOfImages: Int
    
    private var content: Content
    
    @EnvironmentObject var currentIndex: CurrentIndex
    
    init(numberOfImages: Int, @ViewBuilder content: () -> Content) {
        self.numberOfImages = numberOfImages
        self.content = content()
    }

    
    private let timer = Timer.publish(every: 3, on: .main, in: .common).autoconnect()
        
    var body: some View {
        GeometryReader { geometry in
            HStack(spacing: 0) {
                self.content
            }
            .frame(width: geometry.size.width,
                   height: geometry.size.height,
                   alignment: .leading)
            .offset(x: CGFloat(self.currentIndex.index) * -geometry.size.width, y: 0)
            .animation(.spring())
            .onReceive(self.timer) { _ in
                let numberOfImages = self.numberOfImages == 0 ? 1 : self.numberOfImages
                self.currentIndex.index = (self.currentIndex.index + 1) % numberOfImages
            }
        }
    }
}

#if DEBUG
struct ImageCarouselView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { geometry in
            ImageCarouselView(numberOfImages: 3) {
                Image("generation-i")
                    .resizable()
                    .scaledToFill()
                    .frame(width: geometry.size.width,
                           height: geometry.size.height)
                    .clipped()
                Image("generation-ii")
                    .resizable()
                    .scaledToFill()
                    .frame(width: geometry.size.width,
                           height: geometry.size.height)
                    .clipped()
                Image("generation-iii")
                    .resizable()
                    .scaledToFill()
                    .frame(width: geometry.size.width,
                           height: geometry.size.height)
                    .clipped()
            }
        }.frame(width: UIScreen.main.bounds.width,
                height: 300,
                alignment: .center)
    }
}
#endif
