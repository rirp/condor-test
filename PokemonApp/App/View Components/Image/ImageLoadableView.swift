//
//  ImageLoadableView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 14/04/21.
//

import SwiftUI

struct ImageLoadableView<Placeholder: View> : View {
    @StateObject private var loader: ImageLoader
    private let placeholder: Placeholder
    private let image: (UIImage) -> Image
    
    init(
        url: URL,
        @ViewBuilder placeholder: () -> Placeholder,
        @ViewBuilder image: @escaping (UIImage) -> Image = Image.init(uiImage:)
    ) {
        self.placeholder = placeholder()
        self.image = image
        
        /// Setup loader
        let imageLoader = ImageLoader(url: url,cache: Environment(\.imageCache).wrappedValue)
        _loader = StateObject(wrappedValue: imageLoader)
    }
    
    var body: some View {
        content.onAppear(perform: loader.load)
    }
    
    private var content: some View {
        Group {
            if loader.image != nil {
                image(loader.image!)
                    .resizable()
                    .scaledToFit()
            } else {
                placeholder
            }
        }
    }
}

//#if DEBUG
//struct ImageLoadableView_Previews: PreviewProvider {
//    static var previews: some View {
//        ImageLoadableView(with: PokemonApiURL.image("1").endpoint())
//    }
//}
//#endif
