//
//  ImageCache+EnvironmentKey.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 30/04/21.
//

import SwiftUI

struct ImageCacheKey: EnvironmentKey {
    static let defaultValue: ImageCache = TemporaryImageCache()
}

extension EnvironmentValues {
    var imageCache: ImageCache {
        get { self[ImageCacheKey.self] }
        set { self[ImageCacheKey.self] = newValue }
    }
}
