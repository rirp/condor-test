//
//  PageIndicatorView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 25/04/21.
//

import SwiftUI

// MARK: - Dot Indicator -
struct DotIndicatorView: View {
    let minScale: CGFloat = 1
    let maxScale: CGFloat = 1.1
    let minOpacity: Double = 0.6
    
    let pageIndex: Int
    @Binding var selectedPage: Int
    
    var body: some View {
        
        Button(action: {
            self.selectedPage = self.pageIndex
        }) {
            Circle()
                .scaleEffect(
                    selectedPage == pageIndex
                        ? maxScale
                        : minScale
                )
                .animation(.spring())
                .foregroundColor(
                    selectedPage == pageIndex
                        ? Color.white
                        : Color.gray.opacity(minOpacity)
                )
        }
    }
}

// MARK: - Previews -
#if DEBUG
struct DotIndicator_Previews: PreviewProvider {
    static var previews: some View {
        DotIndicatorView(pageIndex: 0, selectedPage: .constant(0))
            .previewLayout(.fixed(width: 200, height: 200))
            .previewDisplayName("Hello")
    }
}
#endif
