//
//  PokemonTypeView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 20/04/21.
//

import SwiftUI

struct TypesView: View {
    @ObservedObject var types: TypeListObservable
    
    var body: some View {
        HStack(alignment: .center) {
            ForEach(types.types, id: \.self) { type in
                Text(type.value())
                    .font(.headline)
                    .padding(.vertical, 5)
                    .padding(.horizontal)
                    .background(Color(type.value()))
                    .foregroundColor(.white)
                    .cornerRadius(25)
            }
        }
    }
}

#if DEBUG
struct PokemonTypeView_Previews: PreviewProvider {
    static var previews: some View {
        TypesView(types: TypeListObservable(types: mockTypes))
    }
}
#endif
