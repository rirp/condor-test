//
//  Color.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 30/04/21.
//

import SwiftUI

extension Color {
    static var types: [Color] {
        [
            Color("bug"), Color("dark"), Color("dragon"), Color("electric"),
            Color("fairy"), Color("figthing"), Color("fire"), Color("flying"),
            Color("ghost"), Color("grass"), Color("ground"), Color("ice"),
            Color("normal"), Color("poison"), Color("psychic"), Color("rock"),
            Color("steel"), Color("water")
        ]
    }
}
