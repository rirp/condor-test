//
//  StringProtocol.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 20/04/21.
//

import Foundation

extension StringProtocol {
    var firstCapitalized: String { return prefix(1).capitalized + dropFirst() }
}
